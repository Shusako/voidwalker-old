package info.shusako;

import info.shusako.display.main.DisplayMain;
import info.shusako.display.main.MainMenu;
import org.newdawn.slick.*;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.command.Command;
import org.newdawn.slick.command.InputProviderListener;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Shusako on 8/5/2016.
 * Made for project Voidwalker
 */
public class Voidwalker extends BasicGame implements InputProviderListener {

    private static DisplayMain currentDisplayMenu;
    public static void setDisplayMenu(DisplayMain displayMenu) {
        if(currentDisplayMenu != null) {
            currentDisplayMenu.onExit();
        }
        currentDisplayMenu = displayMenu;
        currentDisplayMenu.init(apc);
    }

    private static int deltaTickLimit;

    public static void setTicksPerSecond(int ticksPerSecond) {
        Voidwalker.deltaTickLimit = 1000 / ticksPerSecond;
    }

    private Voidwalker(String title) {
        super(title);
    }

    @Override
    public void init(GameContainer gameContainer) throws SlickException {
        gameContainer.setShowFPS(true);

        setTicksPerSecond(20);
        setDisplayMenu(new MainMenu());
    }

    private int deltaCounter;

    @Override
    public void update(GameContainer gameContainer, int delta) throws SlickException {
        deltaCounter += delta;

        for(; deltaCounter >= deltaTickLimit; deltaCounter -= deltaTickLimit) {
            currentDisplayMenu.update(gameContainer);
        }
    }

    @Override
    public void mouseClicked(int button, int x, int y, int clickCount) {
        currentDisplayMenu.onMouseClicked(x, y);
    }

    @Override
    public void render(GameContainer gameContainer, Graphics graphics) throws SlickException {
        currentDisplayMenu.render(gameContainer, graphics);

        int i = 2;
        int space = 11;
        graphics.setColor(Color.black);
        Input input = gameContainer.getInput();
        graphics.drawString("MouseX: " + input.getMouseX(), 10, i * space); i ++;
        graphics.drawString("MouseY: " + input.getMouseY(), 10, i * space); i ++;
    }

    private static AppGameContainer apc;

    public static void main(String[] args) {
        try {
            apc = new AppGameContainer(new Voidwalker("Voidwalker"));
            apc.setDisplayMode(1280, 720, false);
            apc.start();
        } catch (SlickException e) {
            Logger.getLogger(Voidwalker.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    @Override
    public void controlPressed(Command command) {

    }

    @Override
    public void controlReleased(Command command) {

    }
}
