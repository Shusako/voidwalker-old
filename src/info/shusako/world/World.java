package info.shusako.world;

import info.shusako.entity.Entity;
import info.shusako.entity.EntityRock;
import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shusako on 8/7/2016.
 * For project Voidwalker, 2016
 */
public class World {

    private final int TILE_SIZE = 16;

    private int[][] world;
    private List<Entity> entityList;

    public void init() {
        world = new int[64][64];
        for(int i = 0; i < world.length; i ++) {
            for(int j = 0; j < world[i].length; j ++) {
                world[i][j] = ((255 & 0xFF) << 24) |
                        (((i * 4) & 0xFF) << 16) |
                        (((j * 4) & 0xFF) << 8);
//                world[i][j] = (((i << 16) & 0xFF) | ((j << 8) & 0xFF));
            }
        }

        entityList = new ArrayList<>();
        EntityRock rock = new EntityRock();
        rock.posX = 15 * TILE_SIZE;
        rock.posY = 8 * TILE_SIZE;
        entityList.add(rock);
    }

    public void update() {
        entityList.forEach(Entity::update);
    }

    private float posX, posY;

    public void render(Graphics graphics) {
        for(int i = 0; i < world.length; i ++) {
            for(int j = 0; j < world[i].length; j ++) {
                graphics.setColor(new Color(world[i][j]));
                graphics.fillRect(i * TILE_SIZE - posX, j * TILE_SIZE - posY, TILE_SIZE, TILE_SIZE);
            }
        }

        for(Entity entity : entityList) {
            entity.render(graphics);
        }
    }
}
