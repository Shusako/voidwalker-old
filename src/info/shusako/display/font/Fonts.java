package info.shusako.display.font;

import org.newdawn.slick.TrueTypeFont;

import java.awt.*;

/**
 * Created by Shusako on 8/5/2016.
 * For project Voidwalker, 2016
 */
public class Fonts {

    public static TrueTypeFont componentFont = new TrueTypeFont(new Font("Calibri", Font.TRUETYPE_FONT, 36), true);
}
