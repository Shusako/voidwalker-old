package info.shusako.display.components;

import info.shusako.display.font.Fonts;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;

/**
 * Created by Shusako on 8/5/2016.
 * For project Voidwalker, 2016
 */
public class Button extends Component {

    private String name;
    private Runnable runnable;

    private boolean mouseOver;
    private int mouseOverCounter;

    private final int EXTEND_COUNT = 20;

    public Button(String name, int posX, int posY, int width, int height, Runnable runnable) {
        super(posX, posY, width, height);
        this.name = name;
        this.runnable = runnable;
    }

    @Override
    public void update(GameContainer gameContainer) {
        Input input = gameContainer.getInput();
        mouseOver = isInBounds(input.getMouseX(), input.getMouseY());

        if(mouseOver) mouseOverCounter ++;
        else mouseOverCounter --;
        if(mouseOverCounter < 0) mouseOverCounter = 0;
        if(mouseOverCounter > EXTEND_COUNT) mouseOverCounter = EXTEND_COUNT;
    }

    @Override
    public void onMouseClicked(int x, int y) {
        this.runnable.run();
    }

    @Override
    public void render(Graphics graphics) {
        graphics.setColor(new Color(0F, 0F, 0F, 0.5F));
        int expand = Math.min(mouseOverCounter, EXTEND_COUNT);
        graphics.fillRect(this.posX - expand, this.posY/* - expand*/, this.width + (expand * 2), this.height/* + (expand * 2)*/);

        Fonts.componentFont.drawString(posX + (width / 2) - (Fonts.componentFont.getWidth(name) / 2), posY + (height / 2) - (Fonts.componentFont.getHeight("A") / 2) + 2, name, mouseOver ? Color.red : Color.white);
    }
}
