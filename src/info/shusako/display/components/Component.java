package info.shusako.display.components;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

/**
 * Created by Shusako on 8/5/2016.
 * For project Voidwalker, 2016
 */
public class Component {

    int posX, posY;
    int width, height;

    Component(int posX, int posY, int width, int height) {
        this.posX = posX;
        this.posY = posY;
        this.width = width;
        this.height = height;
    }

    boolean isInBounds(int x, int y) {
        return x >= posX && x < (posX + width) && y >= posY && y < (posY + height);
    }

    public void update(GameContainer gameContainer) {}
    public void onMouseClicked(int x, int y) {}
    public void render(Graphics graphics) {}
}
