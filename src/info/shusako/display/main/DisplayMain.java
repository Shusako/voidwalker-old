package info.shusako.display.main;

import info.shusako.display.components.Component;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shusako on 8/5/2016.
 * For project Voidwalker, 2016
 */
public class DisplayMain {

    private List<Component> components = new ArrayList<>();

    @SuppressWarnings("all") // this cannot be package-private
    protected void registerComponent(Component component) {
        this.components.add(component);
    }

    public void init(GameContainer gameContainer) {}

    public void update(GameContainer gameContainer) {
        for(Component component : components) {
            component.update(gameContainer);
        }
    }

    public void onMouseClicked(int x, int y) {
        for(Component component : components) {
            component.onMouseClicked(x, y);
        }
    }

    public void render(GameContainer gameContainer, Graphics graphics) {
        for(Component component : components) {
            component.render(graphics);
        }
    }

    public void onExit() {}
}
