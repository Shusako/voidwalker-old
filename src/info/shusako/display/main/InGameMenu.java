package info.shusako.display.main;

import info.shusako.Voidwalker;
import info.shusako.world.World;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

/**
 * Created by Shusako on 8/7/2016.
 * For project Voidwalker, 2016
 */
public class InGameMenu extends DisplayMain {

    private World world; // TODO: DO NOT KEEP THIS HERE, THE GUI SHOULD NOT HOLD WORLD INFORMATION

    @Override
    public void init(GameContainer gameContainer) {
        Voidwalker.setTicksPerSecond(20);
        world = new World();
        world.init();
    }

    @Override
    public void update(GameContainer gameContainer) {
        super.update(gameContainer);
        world.update();
    }

    @Override
    public void onMouseClicked(int x, int y) {
        super.onMouseClicked(x, y);
    }

    @Override
    public void render(GameContainer gameContainer, Graphics graphics) {
        world.render(graphics);
        super.render(gameContainer, graphics);
    }
}
