package info.shusako.display.main;

import info.shusako.Voidwalker;
import info.shusako.display.components.Button;
import info.shusako.display.components.Component;
import org.newdawn.slick.*;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Shusako on 8/5/2016.
 * For project Voidwalker, 2016
 */
public class MainMenu extends DisplayMain {

    private Image background;
    private Image title;

    @Override
    public void init(GameContainer gameContainer) {
        Voidwalker.setTicksPerSecond(60);

        try {
            background = new Image("res/background-blur.png");
            title = new Image("res/title.png");
        } catch (SlickException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }

        int halfWidth = 150;
        Component button = new Button("Play", gameContainer.getWidth() / 2 - halfWidth, title.getHeight() + 25 + 50, halfWidth * 2, 60, new Runnable() {
            @Override
            public void run() {
                System.out.println("Works");
                Voidwalker.setDisplayMenu(new InGameMenu());
            }
        });
        this.registerComponent(button);
    }

    @Override
    public void render(GameContainer gameContainer, Graphics graphics) {
        background.draw(0, 0, gameContainer.getWidth(), gameContainer.getHeight());

        title.draw(gameContainer.getWidth() / 2 - title.getWidth() / 2, 25, title.getWidth(), title.getHeight());

        super.render(gameContainer, graphics); // renders the components
    }
}
