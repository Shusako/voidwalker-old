package info.shusako.entity;

import org.newdawn.slick.Graphics;

/**
 * Created by Shusako on 8/7/2016.
 * For project Voidwalker, 2016
 */
public abstract class Entity {

    public float posX, posY;

    public abstract void update();
    public abstract void render(Graphics graphics);
}
