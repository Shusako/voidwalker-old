package info.shusako.entity;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Graphics;

/**
 * Created by Shusako on 8/7/2016.
 * For project Voidwalker, 2016
 */
public class EntityRock extends Entity {

    @Override
    public void update() {
        float speed = 7.5F;
        if(Keyboard.isKeyDown(Keyboard.KEY_W)) {
            posY -= speed;
        }
        if(Keyboard.isKeyDown(Keyboard.KEY_S)) {
            posY += speed;
        }
        if(Keyboard.isKeyDown(Keyboard.KEY_A)) {
            posX -= speed;
        }
        if(Keyboard.isKeyDown(Keyboard.KEY_D)) {
            posX += speed;
        }
    }

    @Override
    public void render(Graphics graphics) {

    }
}
